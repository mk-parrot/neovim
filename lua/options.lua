require "nvchad.options"

-- add yours here!

local o = vim.o
-- o.cursorlineopt ='both' -- to enable cursorline!

o.clipboard = ""
vim.wo.foldlevel = 99
vim.wo.foldmethod = 'expr'
vim.wo.foldexpr = 'v:lua.vim.treesitter.foldexpr()'
vim.wo.foldtext = "v:lua.vim.treesitter.foldtext()"
vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
